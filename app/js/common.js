$(function () {

  $(window).scroll(function() {
    if ($(this).scrollTop() > 250){
      $('.top-line').addClass("sticky");
    }
    else{
      $('.top-line').removeClass("sticky");
    }
  });

  $('#WAButton').floatingWhatsApp({
    phone: '+77064060066', //WhatsApp Business phone number
    headerTitle: 'WhatsApp чат!', //Popup Title
    popupMessage: 'Здравствуйте, чем я могу вам помочь?', //Popup Message
    showPopup: true, //Enables popup display
    buttonImage: '<img src="../libs/floating-whatsapp-master/whatsapp.svg" />', //Button Image
    //headerColor: 'crimson', //Custom header color
    //backgroundColor: 'crimson', //Custom background button color
    position: "right" //Position: left | right

  });

  $('.slider4').magnificPopup({
		delegate: 'a',
		type: 'image',
		tLoading: 'Loading image #%curr%...',
		mainClass: 'mfp-img-mobile',
		gallery: {
			enabled: true,
			navigateByImgClick: true,
			preload: [0,1] // Will preload 0 - before current, and 1 after the current image
		},
		image: {
			tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
		}
	});


  $(".lamps__txt_aciton button").on("click", function (e) {
    setTimeout(function () {
      $.magnificPopup.open({
        items: {
          src: "#callback", // can be a HTML string, jQuery object, or CSS selector
          type: "inline",
        },
      });
    }, 0);
  });

  $(".lamps___items_modal .lamps__wrapper").not(":first").hide();
  $(".lamps___items_modal .lamps__tabs li").first().addClass("active");
  $(".lamps___items_modal .lamps__tabs li")
    .click(function () {
      $(".lamps___items_modal .lamps__tabs li")
        .removeClass("active")
        .eq($(this).index())
        .addClass("active");
      $(".lamps___items_modal .lamps__wrapper")
        .hide()
        .eq($(this).index())
        .fadeIn();
    })
    .eq(0)
    .addClass("active");

  $(".lamps__thmub_img ul li").on("click", function (e) {
    e.preventDefault();
    // alert('Works');
    let thmubImg = $(this).find("img").attr("src");
    $(".lamps__thmub_img ul li").removeClass("active");
    $(this).addClass("active");
    $(".bigImg").attr("src", thmubImg);
  });

  $(".lamps__items").each(function (e) {
    let th = $(this);

    th.attr("href", "#lamp__items-" + e)
      .find(".lamps___items_modal")
      .attr("id", "lamp__items-" + e);
  });

  $(".lamps__items").magnificPopup({
    type: "inline",

    fixedContentPos: true,

    removalDelay: 300,
    mainClass: "my-mfp-zoom-in",
  });

  $(".hamburger").on("click", function (e) {
    e.preventDefault();
    $(this).toggleClass("is-active");
    if ($(".hamburger").hasClass("is-active")) {
      $(".main-menu").addClass("main-mnu-active");
    } else {
      $(".main-menu").removeClass("main-mnu-active");
    }
  });

  $(".main-menu ul li a").on("click", function (e) {
    $(".hamburger").removeClass("is-active");
    $(".main-menu").removeClass("main-mnu-active");
  });

  $(".about__carousel").owlCarousel({
    items: 1,
    loop: false,
    nav: true,
    navText: [
      "<i class='fa fa-angle-left' aria-hidden='true'></i>",
      "<i class='fa fa-angle-right' aria-hidden='true'></i>",
    ],
  });

  // Owl-Carousel;
  $(".partners-wrap").owlCarousel({
    loop: true,
    autoplay:true,
    autoplayTimeout:1500,
    autoplayHoverPause:true,
    items: 6,
    nav: true,
    navText: [
      "<i class='fa fa-arrow-left' aria-hidden='true'></i>",
      "<i class='fa fa-arrow-right' aria-hidden='true'></i>",
    ],
    touchDrag: true,
    responsive: {
      0: {
        items: 1,
      },
      600: {
        items: 1,
      },
      1000: {
        items: 6,
      },
    },
  });


  // MagnificPopup
  $(
    'a[href*="#callback"], a[href*="#scheme-open"], .callback-wrapper'
  ).magnificPopup({
    removalDelay: 500,
    callbacks: {
      beforeOpen: function () {
        this.st.mainClass = this.st.el.attr("data-effect");
      },
    },
    fixedContentPos: true,
  });

  $("#phone, #callback-phone").mask("+7(999) 999-99-99", { placeholder: "+7 (   )   -  -  " });

  //E-mail Ajax Send
  $("#callback-form, #forms-footer").submit(function () {
    //Change
    var th = $(this);
    $.ajax({
      type: "POST",
      url: "http://100led.kz/wp-content/themes/sdk/mail.php", //Change
      data: th.serialize(),
    }).done(function () {
      alert("Спасибо за заявку!");
      setTimeout(function () {
        // Done Functions
        th.trigger("reset");
      }, 1000);
    });
    return false;
  });
});

$(".slider1").owlCarousel({
  items: 1,
  loop: true,
  margin: 10,
  nav: true,
  pagination: true,
  navText: [
    '<i class="fa fa-arrow-left" aria-hidden="true"></i>',
    '<i class="fa fa-arrow-right" aria-hidden="true"></i>',
  ],
});

$(".slider2")
  .on("initialized.owl.carousel changed.owl.carousel", function (e) {
    if (!e.namespace) {
      return;
    }
    var carousel = e.relatedTarget;
    $(".slider-counter").text(
      carousel.relative(carousel.current()) + 1 + " / " + carousel.items().length
    );
  })
  .owlCarousel({
    loop: true,
    margin: 30,
    nav: false,
    center: false,

    responsive: {
      0: {
        items: 1,
      },
      600: {
        items: 1,
      },
      1000: {
        items: 3,
      },
    },
  });

checkClasses();
$(".slider2").on("translated.owl.carousel", function (event) {
  checkClasses();
});

function checkClasses() {
  var total = $(".slider2-wrap .owl-stage .owl-item.active").length;

  $(".slider2-wrap .owl-stage .owl-item").removeClass(
    "firstActiveItem lastActiveItem"
  );

  $(".slider2-wrap .owl-stage .owl-item.active").each(function (index) {
    if (index === 0) {
      // this is the first one
      $(this).addClass("firstActiveItem");
    }
    if (index === total - 1 && total > 1) {
      // this is the last one
      $(this).addClass("lastActiveItem");
    }
  });
}


$('.prev-slide').on('click', function() {
  $(".slider2").trigger('prev.owl.carousel');
});

$('.next-slide').on('click', function() {
  $(".slider2").trigger('next.owl.carousel');
});

$(".slider4").owlCarousel({
  items: 4,
  loop: true,
  margin: 30,
  nav: true,
  pagination: true,
  navText: [
    '<i class="fa fa-arrow-left" aria-hidden="true"></i>',
    '<i class="fa fa-arrow-right" aria-hidden="true"></i>',
  ],
  responsive: {
    0: {
      items: 1,
      center: true,
    },
    600: {
      items: 3,
    },
    1000: {
      items: 4,
      center: true,
    },
  },
});
